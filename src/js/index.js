/**
 * 
 * @summary: Main React injection point
 * 
 */

// React modules
import React from 'react';
import ReactDOM from 'react-dom';
import 'babel-polyfill';
import 'stylus/app.styl';

// Redux modules & config
import { Provider } from 'react-redux';
import { createStore } from 'redux'; 
import reducers from './reducers';
const store = createStore(reducers, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

// Main react container component
import App from './components';

ReactDOM.render(
  <Provider store={store}>
    <App />
   </Provider>, 
  document.getElementById('root')
);
