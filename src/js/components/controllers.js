/**
 * 
 * @class: Handle user play selection
 * 
 */

// React/Redux modules
import React, {Component} from 'react'
import { connect } from 'react-redux';
import 'stylus/controller.styl'

// Actions
import { spinAllWheels } from '../actions';

// Map call to wheel spin action to component
const mapDispatchToProps = dispatch => {
	return {
		dispatchPlayAction: () => dispatch(spinAllWheels())
	}
} 

class Controllers extends Component {

	constructor() {
		super();
		this.state = {
			hasBeenClicked: false,
			disabled: false
		}
	}

	render() {

		return (
		<section className="controller-wrapper">
			<button disabled={this.state.disabled} className="play" onClick={() => this.props.dispatchPlayAction()}> PLAY </button>
		</section>
		)
	}

}

export default connect(null, mapDispatchToProps)(Controllers);