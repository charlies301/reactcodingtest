/**
 * 
 * @summary: 
 * 
 */

import React, { Component } from 'react';
import IconImage from 'images/icon.png';

// Application module
import ScoreTracker from './scoreTracker';
import SpinnerMain from './spinner/spinnerMain';
import Controllers from './controllers';


export default class App extends Component {
  
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className="app-wrapper" className="container">

      <header className='hero'>
        <h1>FRUIT MACHINE</h1>
      </header>

      <main>
        <ScoreTracker />
        <SpinnerMain />
        <Controllers />
      </main>

      </div> 

    )
  }
}

