/**
 * 
 * @class: Individual spinner and logic class
 * 
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RED, GREEN, BLUE, YELLOW } from '../../enums';
import { updateSpinnerState } from '../../actions';
import 'stylus/spinner.styl';

// map spin state ans redraw boolean to props
const mapStateToProps = reduxState => {
	return {
		spinCalled: reduxState.controller.spinWheel,
		redraw: reduxState.spinnerState.redrawSpinner
	}
}

// map spin state update action to props
const mapDispatchToProps = dispatch => {
	return {
		dispatchSpinUpdateAction: (spinnerID, colorValue) => dispatch(updateSpinnerState(spinnerID, colorValue))
	}
}

class Spinner extends Component {

	/**
	 * 
	 * @constructor: set class defaults for spinnner
	 * 
	 */
	constructor() {
		super();
		this.state = {
			selectedColor: null,
		  color: ''
		}
		this.spinSettings = {
			spinAngleStart: null,
			spinArcStart: 10,
			spinTime: 0,
			spinTimeTotal: 0,
			spinTimeout: null
		}
		this.quickColorDisplay = null;
		this.arc = Math.PI / 2;
		this.startAngle = 0;
		this.ctx;
		this.spinnerColors = [RED,GREEN,BLUE,YELLOW];
		this.spinnerValues = ['RED','GREEN','BLUE','YELLOW']; 
	}

	/**
	 * 
	 * @method: Draw spinners on canvas once component 
	 * has mounted
	 * 
	 */
	componentDidMount() {
		this._drawSpinner();
	}

	/**
	 * 
	 * @method: Invoke spinner if arg set to true or redraw true
	 * set spinner back to intial state
	 * @param {Object} nextProps 
	 */
	componentWillReceiveProps(nextProps) {
		// spin if boolean true
		if (nextProps.spinCalled) {
			this._spin();
		}
		// redraw if boolean true
		if (nextProps.redraw) {
			this._drawSpinner();
		}
	}


	/**
	 * 
	 * @method: Draw 4 colored spinner to selected canvas 
	 * passed from component props
	 * 
	 */
	_drawSpinner() {
		let canvas = document.getElementsByClassName(this.props.canvasID)[0];
		if(canvas.getContext) {
			
			//set spinner circle and text radius 
			let outsideRadius = 60;
			let insideRadius = 30;
			let textRadius = 30;


			// set spinner stroke and line width
			this.ctx = canvas.getContext('2d');
			this.ctx.clearRect(0,0,200,200);
			this.ctx.strokeStyle = 'black';
			this.ctx.lineWidth = 2;

			// fill spinner quarters with RGBY 
			for(let i = 0; i < this.spinnerColors.length; i++) {
				
				// set arc and select/fill with array color
				let angle = this.startAngle + i * this.arc;
				this.ctx.fillStyle = this.spinnerColors[i];
				
				// draw color qaurter
				this.ctx.beginPath();
				this.ctx.arc(100, 100, outsideRadius, angle, angle + this.arc, false);
      	this.ctx.arc(100, 100, insideRadius, angle + this.arc, angle, true);
      	this.ctx.stroke();
      	this.ctx.fill();

				this.ctx.save();
				this.ctx.shadowOffsetX = -1;
				this.ctx.shadowOffsetY = -1;
				this.ctx.shadowBlur    = 0;
				this.ctx.shadowColor   = "rgb(220,220,220)";
				this.ctx.fillStyle = "black";
				this.ctx.restore();

				// draw selection arrow
				this._drawLandingArrow(outsideRadius);

			}
		}
	}

	/**
	 * 
	 * @method: Draw landing arrow at spinner bottom for 
	 * visual indication of winnning number
	 * 
	 */
	_drawLandingArrow(outsideRadius) {
		this.ctx.fillStyle = 'black';
		this.ctx.beginPath();
		this.ctx.moveTo(100 - 4, 100 - (outsideRadius + 5));
    this.ctx.lineTo(100 + 4, 100 - (outsideRadius + 5));
    this.ctx.lineTo(100 + 4, 100 - (outsideRadius - 5));
    this.ctx.lineTo(100 + 9, 100 - (outsideRadius - 5));
    this.ctx.lineTo(100 + 0, 100 - (outsideRadius - 13));
    this.ctx.lineTo(100 - 9, 100 - (outsideRadius - 5));
    this.ctx.lineTo(100 - 4, 100 - (outsideRadius - 5));
    this.ctx.lineTo(100 - 4, 100 - (outsideRadius + 5));
    this.ctx.fill();
	}

	/**
	 * 
	 * @method: Declare spinner animation settings
	 * 
	 */
	_spin() {
		let [a,b,c,d] = this.props.randomSpins;
		this.spinSettings.spinAngleStart = Math.random() * a + b;
		this.spinSettings.spinTime = 0;
		this.spinSettings.spinTimeTotal = Math.random() * c + d * 1000; 
		this._updateColorIndicator('#ffffff');
		this._rotateWheel();
		
	}

	/**
	 * 
	 * @method: Rotate spinnner
	 * 
	 */
	_rotateWheel() {
		this.spinSettings.spinTime += 30; 

		// stop wheel rotation wheel animation if spin time exceeds 
		// total spin time setting
		if (this.spinSettings.spinTime >= this.spinSettings.spinTimeTotal) {
			return this._stopRotatingWheel();
		}

		let spinAngle = this.spinSettings.spinAngleStart - this._easeout(this.spinSettings.spinTime,
																															0, 
																															this.spinSettings.spinAngleStart,
																															this.spinSettings.spinTimeTotal)
		this.startAngle += (spinAngle * Math.PI / 180);
		this._drawSpinner();
		this.spinSettings.spinTimeout = setTimeout(() => { this._rotateWheel() },30);
	}

	/**
	 * 
	 * @method: Stop rotation of wheel and get index
	 * 
	 */
	_stopRotatingWheel() {
		clearTimeout(this.spinSettings.spinTimeout);
		var degrees = this.startAngle * 180 / Math.PI + 90;
		var arcd = this.arc * 180 / Math.PI;
  	var index = Math.floor((360 - degrees % 360) / arcd);
  	this.ctx.save();
		this._udpateSpinnerState(index);
	}

	/**
	 * 
	 * @method: Get color enum and spinner ID to update 
	 * specific spin color
	 * @param {*} landingIndex 
	 */
	_udpateSpinnerState(landingIndex) {
		let selectedColor = this.spinnerColors[landingIndex];
		let colorEnum = this.spinnerValues[landingIndex];
		this.props.dispatchSpinUpdateAction(this.props.canvasID, colorEnum);
		this._updateColorIndicator(selectedColor);
	}

	/**
	 * Update underside color box to match color wheel landed on
	 * @param {String} color 
	 */
	_updateColorIndicator(color) {
		let element = document.getElementsByClassName('colorbox-' + this.props.canvasID)[0];
		element.style.backgroundColor = color;
	}


	/**
	 * 
	 * @method: easing animation for spinning wheel
	 * 
	 */
	_easeout(t,b,c,d) {
  var ts = (t/=d)*t;
  var tc = ts*t;
  return b+c*(tc + -3*ts + 3*t);	
}

	render() {
		return (
			<div className="spinner"> 
				<canvas className={this.props.canvasID} width="200" height="200">
					Canvas not supported - please try another browser
				</canvas>
				<input disabled className={'box colorbox-' + this.props.canvasID} value={this.state.color} />
			</div>	
		);
	}

}

export default connect(mapStateToProps, mapDispatchToProps)(Spinner)

