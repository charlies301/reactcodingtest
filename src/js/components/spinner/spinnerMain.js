/**
 * 
 * @class: Main class to hold three spinners and state
 * 
 */

import React, { Component } from 'react';
import Spinner from './spinner';

export default class SpinnerMain extends Component {

	render() {
		return (
			<section className="spinner-wrapper">
				<Spinner canvasID={'spinner1'} randomSpins={[10,12,7,8]}/>
				<Spinner canvasID={'spinner2'} randomSpins={[16,30,2,5]}/>
				<Spinner canvasID={'spinner3'} randomSpins={[8,22,2,8]}/>
			</section>	
		)
	}

}