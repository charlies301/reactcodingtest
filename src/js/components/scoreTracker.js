/**
 * 
 * @class: Displays users win / loss points 
 * 
 */

import React, { Component } from 'react';
import { connect } from 'react-redux';
import 'stylus/scores.styl';

// component action creators
import { redrawSpinners } from '../actions';

// map spinner outcomes to component
const mapStateToProps = reduxState => {
	return {
		spinner1: reduxState.spinnerState.spinner1,
		spinner2: reduxState.spinnerState.spinner2,
		spinner3: reduxState.spinnerState.spinner3,
		isSpinning: reduxState.controller.spinWheel
	}
}

// map redraw action to ScoreTracker
const mapDispatchToProps = dispatch => {
	return {
		redrawAction: () => dispatch(redrawSpinners())
	}
}

export class ScoreTracker extends Component {

	constructor() {
		super();
		this.state = {
			count: 0,
			wins: 0,
			losses: 0,
			outcome: ''
		}
	}

	/**
	 * 
	 * @method: Check incoming spinner data for end of spins, spinner3 
	 * is the last spinner to stop. 
	 * @param {Object} nextProps 
	 * 
	 */
	componentWillReceiveProps(nextProps) {
		let spinnerOutcomes = [nextProps.spinner1,nextProps.spinner2,nextProps.spinner3]
		if (spinnerOutcomes.every(outcome => outcome != null)) {
			this._checkResult(nextProps);
		}
	}

	/**
	 * 
	 * @method: check result to identify matching/losing color set and 
	 * update outcome/score counter
	 * @param { Object } props
	 * 
	 */
	_checkResult(props) {
		let results = [props.spinner1,props.spinner2,props.spinner3];
		let randomResult = results[0];
		if (results.every( result => randomResult == result )) {
			this._displayResult('WINNNER', 'wins');
		} else {
			this._displayResult('UNLUCKY', 'losses');
		}
	}

	/**
	 * 
	 * Update score ints on tracker and display either winner/loser then 
	 * action call to redraw spinners
	 * @param {String} outcome 
	 * @param {String} toUpdate 
	 *
	 */
	_displayResult(outcome, toUpdate) {
		let newScore = this.state[toUpdate] + 1;
		this.setState({ 
			outcome,
			[toUpdate]: newScore
	 });
		this.props.redrawAction();
	}


	/**
	 * 
	 * Render score board 
	 * 
	 */
	render(){

		const { wins, losses, outcome } = this.state;

		return (
			<section className="score-wrapper">

				{/* Win counter */}
				<div className="score-box">
					<p className="score-title">Wins</p>
					<p className="score-value">{wins}</p>
				</div>

				{/* Loss counter */}
				<div className="score-box">
					<p className="score-title">Losses</p>
					<p className="score-value">{losses}</p>
				</div>

				{/* Outcome */}
				<div className="score-box">
					<p className="score-title">Outcome</p>
					<p className="score-value">{outcome}</p>
				</div>	
			</section>
		);
	}

}

export default connect(mapStateToProps, mapDispatchToProps)(ScoreTracker)