/**
 * 
 * @summary: Handle spinner state logic
 * 
 */

// application enums 
import { UPDATE_SPINNER_STATE, REDRAW_SPINNERS } from '../actions/types';

// set intial state of spinner outcomes to null
const INITSTATE = {
	spinner1: null,
	spinner2: null, 
	spinner3: null,
	redrawSpinner: false
}

export default (state = INITSTATE, action) => {

	switch(action.type) {

		//update specific spinner based on element ID
		case UPDATE_SPINNER_STATE:
			let { selectedSpinner, spinnerValue } = action.payload
			return {...state, [selectedSpinner]: spinnerValue }

	 //redraw spinners back to original state and set values
	 //for spinners back to null
	 case REDRAW_SPINNERS:
	 			return {...state, 
					 			redrawSpinner: true,
								spinner1: null, 
								spinner2: null, 
								spinner3: null
							}
	
}

	return state;

}