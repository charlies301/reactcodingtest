/**
 * 
 * @summary: Combine reducers into single reducer
 * 
 */

import { combineReducers } from 'redux';
import controllerReducer from './controller';
import spinnerStateReducer from './spinnerState';

const rootReducer = combineReducers({
	controller: controllerReducer,
	spinnerState: spinnerStateReducer
})

export default rootReducer;