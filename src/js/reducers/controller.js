/**
 * 
 * @summary: Handle state for user contoller
 * 
 */

// import type 
import { PLAY, REDRAW_SPINNERS } from '../actions/types';

// set reducer intial state
const INITSTATE = {
	spinWheel: false
}

export default (state = INITSTATE, action) => {

	switch(action.type) {

		case PLAY:
			return {...state, spinWheel: action.payload }

		case REDRAW_SPINNERS:
			return {...state, spinWheel: false }	

	}

	return state;

}