// SCORE ENUMS
export const WIN = 'WIN';
export const LOSE = 'LOSE';
export const RESET = 'RESET';

// CONTROLLER ENUMS
export const PLAY = 'PLAY';

// SPINNNER ENUMS 
export const UPDATE_SPINNER_STATE = 'UPDATE_SPINNER_STATE';
export const REDRAW_SPINNERS = 'REDRAW_SPINNERS';

