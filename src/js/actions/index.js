/**
 * 
 * 
 * @summary: Action creators for application 
 * 
 */

// application enums
import {
	PLAY,
	WIN,
	LOSSES,
	RESET,
	UPDATE_SPINNER_STATE,
	REDRAW_SPINNERS
} from './types';

/**
 * 
 * @action: Call to spin all three wheeels
 * 
 */
export const spinAllWheels = () => {
	return {
		type: PLAY,
		payload: true
	}
}

/**
 * 
 * @action: Update specific spinnner state
 * @param {*} spinnerID 
 * @param {*} colorValue 
 */
export const updateSpinnerState = (spinnerID, colorValue) => {
	return {
		type: UPDATE_SPINNER_STATE,
		payload: {
			selectedSpinner: spinnerID,
			spinnerValue: colorValue
		}
	}
}

/**
 * 
 * @action: Redraw spinner elements back to orginal state
 * 
 */
export const redrawSpinners = () => {
	return {
		type: REDRAW_SPINNERS
	}
}

